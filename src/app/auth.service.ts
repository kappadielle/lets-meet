import { Injectable } from '@angular/core';
import { auth, User } from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { DbService } from './db.service';
import { user } from './user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: any;
  boss$: Observable<any>;

  constructor(private afAuth: AngularFireAuth, private DB: DbService) {
    this.boss$ = this.DB.getDocument(`user/${'eXsNPdClOxWUbPzelzXS9vgUc502'}`).valueChanges();
  }
  checkForUser(): Observable<User> {
    return this.afAuth.authState;
  }
  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.socialSignIn(provider);
  }
  socialSignIn(provider: auth.GoogleAuthProvider) {
    return this.afAuth.auth.signInWithPopup(provider);
    // return this.afAuth.auth.signInWithRedirect(provider);
  }
  getUser(userAuth: User): Observable<user> { // prende documento dell'user, se non c'è lo crea. ritorna Obs dell'user che ha loggato
    const boss = 'eXsNPdClOxWUbPzelzXS9vgUc502';
    const user = {
      id: userAuth.uid, // metti boss se vuoi prova || metti userAuth.uid per esse normale
      name: userAuth.displayName,
      photo: userAuth.photoURL,
      email: userAuth.email,
      phone: userAuth.phoneNumber,
      ref: this.DB.getDocument(`user/${boss}`).ref,
      boss: false,
      pianoStrong: false,
      cards: [{
        numCarta: 0,
        n0: false,
        n1: false,
        n2: false,
        n3: false,
        n4: false,
        n5: false,
        n6: false,
        n7: false,
        n8: false,
        n9: false,
        zDataCreazione: new Date()
      }]
    };
    let man; // observable del document dell'user corrente
    if (user.id !== boss) { // prendi doc della gentaglia
      man = this.DB.getDocument(`user/${boss}/gentaglia/${user.id}`).snapshotChanges().pipe(
        map(snap => {
          let id = null;
          let data = null;
          let currentUser = null;
          if (snap.payload.exists) {
            data = snap.payload.data();
            id   = snap.payload.id;
            currentUser = { id, ...data };
          } else {
            this.DB.getDocument(`user/${boss}/gentaglia/${user.id}`).set(user);
            currentUser = user;
          }
          this.user = currentUser;
          return currentUser;
        }),
        tap(val => console.log(val))
      );
    } else if (user.id === boss) { // prendi doc del boss
      man = this.DB.getDocument(`user/${boss}`).valueChanges()
      .pipe(
        take(1), // toglilo quando il boss avrà bisogno di sapere i dati dagli utenti
        tap(snap => {
          this.user = snap;
        })
      );
    }
    return man;
  }
}
