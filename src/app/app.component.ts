import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { trigger, transition, style, query, group, animate } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('routeAnimations', [
      transition('* <=> *', [
        query(':enter, :leave', style({ position: 'fixed', width: '100%' }), {optional: true}),
        query(':enter', [style({ opacity: 0 })], {optional: true}),
        group([
          query(':leave', [animate('0.4s ease-out', style({ opacity: 0 }))], {optional: true}),
          query(':enter', [
            style({ opacity: 0 }),
            animate('0.4s ease-out', style({ opacity: 1 })),
          ], {optional: true}),
        ]),
      ])
    ])
  ]
})

export class AppComponent {
  title = 'iPush';
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
