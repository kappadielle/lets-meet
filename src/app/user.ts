import { DocumentReference } from 'angularfire2/firestore';
export interface user {
    id: string,
    name: string,
    photo?: string,
    email?: string,
    phone?: string,
    ref: DocumentReference,
    boss: boolean,
    giorniScadenza?: number;
    pianoStrong?: boolean;
    cards?: any[]
}