import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keysUser',  pure: false })
export class KeysUserPipe implements PipeTransform {

  transform(value: any, args: any[] = null): any {
    let keys = [];
    let data;
    for (let key in value) {
      if (key == 'zDataCreazione') {
        data = {key: key, value: new Date(value[key].seconds * 1000)}
      }
      keys.push({key: key, value: value[key]});
    }
    keys = keys.splice(0,10);
    keys.push(data);
    return keys;
    // return Object.keys(value).splice(0,10)//.map(key => value[key]);
  }
  

}
