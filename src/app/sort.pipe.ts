import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(array: any, args?: any): any {
    if (!Array.isArray(array)) {
      return;
    }
    if (!args) {
      return array;
    } else {
      args = args.toUpperCase();
    }
    array.sort((a: any, b: any) => {
      const aname = a.name.toUpperCase();
      const bname = b.name.toUpperCase();
      if (aname.includes(args)  < bname.includes(args)) {
        return 1;
      } else if (aname.includes(args) > bname.includes(args) ) {
        return -1;
      } else {
        return 0;
      }
    });
    return array;
  }

}
