import { Component, OnInit } from '@angular/core';
import { user } from '../user';
import { AuthService } from '../auth.service';
import { DbService } from '../db.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  user: user;

  constructor(private authSer: AuthService, private DB: DbService) { }

  ngOnInit() {
    this.user = this.authSer.user;
  }
  cambiaScadenza(val: number) {
    if (this.user.giorniScadenza !== val) {
      this.user.giorniScadenza = +val;
      this.DB.updateDocument(`user/${this.user.id}`, this.user);
    }
  }
}
