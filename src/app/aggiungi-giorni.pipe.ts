import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'aggiungiGiorni',  pure: false })
export class aggiungiGiorniPipe implements PipeTransform {

  transform(value: Date, args: number): any {
    console.log(args)
    value.setDate(value.getDate() + args);
    console.log(value);
    return value;
  }
  

}
