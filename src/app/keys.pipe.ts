import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys',  pure: false })
export class KeysPipe implements PipeTransform {

  transform(value: any, args: any[] = null): any {
    let keys = [];
    let scadenza;
    for (let key in value) {
      if (key == 'scadenza') {
        scadenza = {key: key, value: value[key]}
      }
      keys.push({key: key, value: value[key]});
    }
    keys = keys.splice(0,10);
    keys.push(scadenza);
    return keys;
    // return Object.keys(value).splice(0,10)//.map(key => value[key]);
  }
  

}
