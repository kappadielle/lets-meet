import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { filter, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  boss: any;
  sub: Subscription;

  constructor(public authSer: AuthService) { }
  ngOnInit() {
    this.sub = this.authSer.boss$.pipe(
      filter(val => val != null)
    ).subscribe(val => {
      this.boss = val;
      console.log(val)
    })
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
