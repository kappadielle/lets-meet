import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CardsComponent } from './cards/cards.component';
import { SettingsComponent } from './settings/settings.component';
const routes: Routes = [
  { path: 'login' , component: LoginComponent , data: {animation: 'page1'} },
  { path: 'home' , component: HomeComponent , data: {animation: 'page2'} },
  { path: 'cards' , component: CardsComponent , data: {animation: 'page3'} },
  { path: 'settings' , component: SettingsComponent , data: {animation: 'page4'} },
  { path: '' , redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
