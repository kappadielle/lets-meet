import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  constructor(private DB: AngularFirestore) { }
  getDocument(val: string): AngularFirestoreDocument {
    return this.DB.doc(val);
  }
  getCollection(collection: string): AngularFirestoreCollection<any> {
    return this.DB.collection(collection);
  }
  /*
  getOrderedCollection(collection: string, fieldPath: string, mod: OrderByDirection = 'desc'): AngularFirestoreCollection<any> {
    return this.DB.collection(collection, ref => ref.orderBy(fieldPath, mod )); // asc default
  }
  getQueryedCollection(collection: string, field: string, operation: WhereFilterOp, val2: any): AngularFirestoreCollection<any> {
    return this.DB.collection(collection, ref => ref.where(field, operation, val2)); // query
  }
  */
  delDocument(path: string) {
    this.getDocument(path).delete();
  }
  updateDocument(path: string, data: any) { // aggiunge campi e aggiorna quelli gia presenti
    this.getDocument(path).update(data)
    .then(() => {console.log(`aggiornato con successo`); })
    .catch(err => console.log(`errore nell'aggiornamento, errore: ${err}`));
  }
}
