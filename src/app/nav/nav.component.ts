import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { mergeMap, filter, takeWhile, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  subUser: Subscription;

  constructor(private authSer: AuthService, private router: Router) { }
  user: any;
  ngOnInit() {
    this.subUser = this.authSer.checkForUser() // qui mi riprendo l'user da firebase.authentication
      .pipe(
        // takeWhile(val => val == null),
        filter(val => val != null),
        mergeMap(user => // qui me lo riprendo dal database
          this.authSer.getUser(user)
        )
      ).subscribe(user => {
        if (user) {
          this.user = user;
          console.log(this.user)
          // this.router.navigate(['/home']);
          // this.subUser.unsubscribe();
          // se lo un-commenti quando il boss timbra, il cliente non riceve in real time
        }
      });
  }
}
