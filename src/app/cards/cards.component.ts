import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DbService } from '../db.service';
import { tap, take, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { user } from '../user';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
  animations: [
    trigger('timbra', [
      state('true', style({
        backgroundColor: '#1DC690',
        color: '#EAEAE0',
        border: '2px solid #1C4670'
      })),
      state('false', style({
        backgroundColor: 'rgb(247, 247, 244)',
        border: '1px solid #1C4670',
        color: '#1C4670'
      })),
      transition('false => true', [
        animate(250)
      ]),
      transition('true => false', [
        animate(250)
      ]),
    ]),
  ]
})
export class CardsComponent implements OnInit {
  user: any;
  gentaglia: Observable<any[]>;
  arrGentaglia: any[];
  ricercaString: string;
  constructor(private authSer: AuthService, private DB: DbService) { }

  ngOnInit() {
      this.user = this.authSer.user;
      this.gentaglia = this.getGentaglia(`${this.user.ref.path}/gentaglia`); // lo prende solo il boss per mo
  }
  getGentaglia(val: string): Observable<any[]> {
    return this.DB.getCollection(val).valueChanges()
    .pipe(
      map(arr => {
        return arr.map(item => {
          return this.aggiungiScadenza_togliScadute(item);
        });
      }),
      // take(1) // ho messo take(1) pekke sennò ad ogni "updatedoc" si refreshava la pag
    );
  }
  aggiungiScadenza_togliScadute(item): any {
    const newCard = {
      n0: false,
      n1: false,
      n2: false,
      n3: false,
      n4: false,
      n5: false,
      n6: false,
      n7: false,
      n8: false,
      n9: false,
      zDataCreazione: new Date(),
      numCarta: item.cards.length,
    }; // aggiungi scadenza e togli card scadute

    const dataCreationLastCard = new Date(item.cards[item.cards.length - 1].zDataCreazione.seconds * 1000); // data della creazione carta
    const giorniCardCreation = dataCreationLastCard.getTime() / (1000 * 60 * 60 * 24);  // num giorni dal 1/1/1970 a data creazione
    const giorniToday = new Date().getTime() / (1000 * 60 * 60 * 24); // num giorni dal 1/1/1970 a oggi
    // tslint:disable-next-line:max-line-length
    item.cards[item.cards.length - 1] = {...item.cards[item.cards.length - 1], scadenza: Math.round(giorniCardCreation - giorniToday  + this.user.giorniScadenza)};
    // item.cards[item.cards.length - 1].scadenza = Math.round(giorniCardCreation - giorniToday  + this.user.giorniScadenza)
    if (giorniToday - giorniCardCreation > this.user.giorniScadenza) { // se scaduta
      item.cards[item.cards.length - 1] = newCard;
    }
    console.log(item.cards[item.cards.length - 1]);
    return item;
  }
  timbraCard(timbro: any, userDoc: any) {
    userDoc.cards[userDoc.cards.length - 1][timbro.key] = !timbro.value; // cambia valore al pulsante cliccato
    const arrKeys = ['n0', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 'n7', 'n8', 'n9'];
    let i = 0;
    for (const item of arrKeys) {
      if (userDoc.cards[userDoc.cards.length - 1][item]) {
        i += 1;
      }
    }
    if (i === 10) { // se card è completata
      userDoc.cards[userDoc.cards.length - 1][timbro.key] = timbro.value; // cambia valore al pulsante cliccato
      userDoc.cards[userDoc.cards.length - 1] = {...userDoc.cards[userDoc.cards.length - 1], clickAndDone: true};
    } else {
      // this.DB.updateDocument(`${this.user.ref.path}/gentaglia/${userDoc.id}`, userDoc) // updata
    }
  }
  completaLastCard(item: user) {
    const card = {
      n0: false,
      n1: false,
      n2: false,
      n3: false,
      n4: false,
      n5: false,
      n6: false,
      n7: false,
      n8: false,
      n9: false,
      zDataCreazione: new Date(),
      numCarta: item.cards.length,
    };
    const completed = {
      n0: true,
      n1: true,
      n2: true,
      n3: true,
      n4: true,
      n5: true,
      n6: true,
      n7: true,
      n8: true,
      n9: true,
      numCarta: item.cards.length - 1,
    };
    item.cards[item.cards.length - 1] = completed;
    item.cards.push(card);
    this.DB.updateDocument(`${this.user.ref.path}/gentaglia/${item.id}`, item); // updata
  }
  cambiaPiano(item) {
    console.log(item);
    item.pianoStrong = !item.pianoStrong;
  }
  aperto(item: user) {
  }
  chiuso(item: user) { // i dati si salvano solo quando si chiude l'expansion panel
    this.DB.updateDocument(`${this.user.ref.path}/gentaglia/${item.id}`, item); // updata
  }
}
